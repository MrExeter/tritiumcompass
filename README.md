**Tritium Compass** as published to The Google Play Store

The compass uses both the on-board accelerometer and magnetic sensors for detecting magnetic north and changes in the phones orientation.

I utilized a low pass filter to filter out excess "noise" events from the motion and magnetic sensors, otherwise the needle would have been very "jittery".

* It features custom graphics created by me using SketchApp.
* It features a rotatable Bezel ring.
* It displays both a numeric heading as well a the cardinal direction.

* It displays an admob banner on the bottom (currently displays example banner) actual app has my admob API number