


/*******************************************************************************
 * Copyright (c) 2015 to present.   JTS Interactive and John Sentz developer.  All rights reserved.
 *
 *
 *  *******************************************************************************
 *  Initial App submission
 *  09-Oct-2015  1.0V
 *
 *
 *  Updates and changes
 *
 *  *******************************************************************************
 *  13-Nov-2015:
 *  Submit update 1.1V
 *
 *  Changed the Sensor sampling rate in the OnCreate() method from SENSOR_DELAY_NORMAL to SENSOR_DELAY_GAME.
 *  The rate was set faster in the OnResume().  Now the compass is more responsive from the start.
 *
 *  Added some velocity dampening in the OnFling() method of the gesture detector to prevent "Roulette Wheel"
 *  spinning effect.
 *  *******************************************************************************
 *
 *
 *
 *
 *
 *
 *
 *
 ******************************************************************************/


package com.jtsinteractive.tritiumcompass;

import android.annotation.TargetApi;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class MainActivity extends AppCompatActivity implements SensorEventListener, View.OnClickListener, NoticeDialogFragment.NoticeDialogListener {


    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor magnetometer;

//    private SimpleEula theEula;

    private static Bitmap imageOriginal, imageScaled;

    private Matrix matrix;

    private Float azimut;

    private ImageView mPointer;
    private ImageView mBezel;
    private TextView mHeadingTextView;
    private int lastHeading;

    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean mLastAccelerometerSet = false;
    private boolean mLastMagnetometerSet = false;
    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];
    private float mCurrentDegree = 0f;

    //    private GestureOverlayView mGestureOverlayView = new GestureOverlayView(this);
    private GestureDetector mGestureDetector;

    // needed for detecting the inversed rotations
    private boolean[] quadrantTouched;
    private boolean allowRotating;

    private int dialerHeight, dialerWidth;

    private StringBuilder mHeadingStrBld;
    private String mCardinalStr;


    static final float ALPHA = .1f;

    //******************************************************************************************
    //
    //  Create tags for action buttons
    //
    private static final String TAG_FACEBOOK = "Facebook";
    private static final String TAG_TWITTER = "Twitter";
    private static final String TAG_LINKEDIN = "Linkedin";
    private static final String TAG_GOOGLE = "googlePlus";



    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        confirmCalibration();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        mHeadingStrBld = new StringBuilder();
        mCardinalStr = new String();

        //******************************************************************************************
        //
        //  Create Ad Banner
        //
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        //******************************************************************************************
        //
        //  Floating Action Button
        //

        //  buildFAB();

        //******************************************************************************************
        //
        //

        mHeadingTextView = (TextView) findViewById(R.id.headingTextView);
        mPointer = (ImageView) findViewById(R.id.tacCompassSpinner);
        mBezel = (ImageView) findViewById(R.id.tacBezel);

        allowRotating = true;

        mGestureDetector = new GestureDetector(this, new MyGestureDetector());


        //******************************************************************************************
        //
        //
        // there is no 0th quadrant, to keep it simple the first value gets ignored
        quadrantTouched = new boolean[] { false, false, false, false, false };


        //##########################################################################################
        //
        if(imageOriginal == null){
            imageOriginal = BitmapFactory.decodeResource(getResources(), R.drawable.outerbezel);

        }

        //##########################################################################################
        //
        //  Initialize the rotation matrix
        //
        if(matrix == null){
            matrix = new Matrix();
        }
        else {
            matrix.reset();
        }


        //******************************************************************************************
        //
        //

        mBezel.setOnTouchListener(new MyOnTouchListener());
        mBezel.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                //******************************************************************************************
                //
                //
                // method called more than once, but the values only need to be initialized one time
                //
                if (dialerHeight == 0 || dialerWidth == 0) {
                    dialerHeight = mBezel.getHeight();
                    dialerWidth = mBezel.getWidth();

                    setDialerHeight(dialerHeight);
                    setDialerWidth(dialerWidth);

                    // resize
                    Matrix resize = new Matrix();
                    resize.postScale((float)Math.min(dialerWidth,
                                    dialerHeight) / (float)imageOriginal.getWidth(),
                                    (float)Math.min(dialerWidth, dialerHeight) / (float)imageOriginal.getHeight());

                    imageScaled = Bitmap.createBitmap(imageOriginal,
                                                    0,
                                                    0,
                                                    imageOriginal.getWidth(),
                                                    imageOriginal.getHeight(),
                                                    resize,
                                                    false);

                    // translate to the image view's center
                    float translateX = dialerWidth / 2 - imageScaled.getWidth() / 2;
                    float translateY = dialerHeight / 2 - imageScaled.getHeight() / 2;
                    matrix.postTranslate(translateX, translateY);

                    mBezel.setImageBitmap(imageScaled);
                    mBezel.setImageMatrix(matrix);
                }
            }
        });
        ActivateSensors();
    }

    private void confirmCalibration() {

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        boolean dontShow = prefs.getBoolean("DON'T SHOW", false);

        if (dontShow == false) {
            DialogFragment newFragment = new NoticeDialogFragment();
            newFragment.show(getFragmentManager(), "Hello");
        }
    }

    private void ActivateSensors() {

        //##########################################################################################
        //
        //  Initialize the sensor manager
        //
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //##########################################################################################
        //
        //  Initialize and register the accelerometer
        //
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer , SensorManager.SENSOR_DELAY_GAME);

        //##########################################################################################
        //
        //  Initialize and register the accelerometer
        //
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onClick(View v) {

        if( v.getTag().equals(TAG_FACEBOOK) ){
            //  Facebook button clicked
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            // add the app link
            intent.putExtra(Intent.EXTRA_TEXT, "http://play.google.com/store/apps/details?  id=com.phonelight.realparrot");
            startActivity(Intent.createChooser(intent, "Share with Facebook"));
        }

        if( v.getTag().equals(TAG_TWITTER) ){
            //  Twitter button clicked

        }

        if( v.getTag().equals(TAG_LINKEDIN) ){
            //  LinkedIn button clicked

        }

        if( v.getTag().equals(TAG_GOOGLE) ){
            //  Google button clicked
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this, accelerometer);
        sensorManager.unregisterListener(this, magnetometer);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float azimuthInRadians = mOrientation[0];
        float azimuthInDegress = (float)(Math.toDegrees(azimuthInRadians)+360)%360;


        float[] tempAccel = new float[3];
        float[] tempMagnetic = new float[3];

        if (event.sensor == accelerometer) {

            mLastAccelerometer = lowPass(event.values.clone(), mLastAccelerometer);
            mLastAccelerometerSet = true;

        } else if (event.sensor == magnetometer) {

            mLastMagnetometer = lowPass(event.values.clone(), mLastMagnetometer);
            mLastMagnetometerSet = true;
        }
        if (mLastAccelerometerSet && mLastMagnetometerSet) {
            SensorManager.getRotationMatrix(mR, null, mLastAccelerometer, mLastMagnetometer);
            SensorManager.getOrientation(mR, mOrientation);

            RotateAnimation ra = new RotateAnimation(
                    mCurrentDegree,
                    -azimuthInDegress,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f);

            ra.setDuration(10);

            ra.setFillAfter(true);

            mPointer.startAnimation(ra);
            mCurrentDegree = -azimuthInDegress;

            int heading = Math.round(azimuthInDegress);

            if(heading == 360){
                heading = 0;
            }

            mCardinalStr = getCardinalHeading(heading);
            mHeadingTextView.setText( String.valueOf(heading) + "˚" + " " + mCardinalStr);
        }
    }

    private String getCardinalHeading(int lastHeading){

        String returnStr = new String("");

        if( lastHeading == 0 ){
            returnStr = "N";
        }
        else if((lastHeading > 0) && (lastHeading < 90) ){
            returnStr = "NE";
        }
        else if(lastHeading == 90 ){
            returnStr = "E";
        }
        else if((lastHeading > 90) && (lastHeading < 180) ){
            returnStr = "SE";
        }
        else if(lastHeading == 180 ){
            returnStr = "S";
        }
        else if((lastHeading > 180) && (lastHeading < 270) ){
            returnStr = "SW";
        }
        else if(lastHeading == 270 ){
            returnStr = "W";
        }
        else if((lastHeading > 270) && (lastHeading <= 360) ){
            returnStr = "NW";
        }

        return returnStr;
    }

    protected float[] lowPass( float[] input, float[] output ) {
        if ( output == null ) {
            return input;
        }

        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }
        return output;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    public int getDialerHeight() {
        return dialerHeight;
    }

    public void setDialerHeight(int dialerHeight) {
        this.dialerHeight = dialerHeight;
    }

    public int getDialerWidth() {
        return dialerWidth;
    }

    public void setDialerWidth(int dialerWidth) {
        this.dialerWidth = dialerWidth;
    }

    private void rotateBezel(float degrees) {
        matrix.postRotate(degrees, dialerWidth / 2, dialerHeight / 2);
        //mBezel.setImageBitmap(Bitmap.createBitmap(imageScaled, 0, 0, imageScaled.getWidth(), imageScaled.getHeight(), matrix, true));
        mBezel.setImageMatrix(matrix);
    }

    private double getAngle(double xTouch, double yTouch) {

        double x = xTouch - (dialerWidth / 2d);
        double y = dialerHeight - yTouch - (dialerHeight / 2d);

        switch (getQuadrant(x, y)) {
            case 1:
                return Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI;
            case 2:
                return 180 - Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI;
            case 3:
                return 180 + (-1 * Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI);
            case 4:
                return 360 + Math.asin(y / Math.hypot(x, y)) * 180 / Math.PI;
            default:
                return 0;
        }
    }

    private static int getQuadrant(double x, double y) {
        if (x >= 0) {
            return y >= 0 ? 1 : 4;
        } else {
            return y >= 0 ? 2 : 3;
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }


    private class MyOnTouchListener implements View.OnTouchListener {

        private double startAngle;

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            //double startAngle = 0;

            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:

                    // reset the touched quadrants
                    for (int i = 0; i < quadrantTouched.length; i++) {
                        quadrantTouched[i] = false;
                    }

                    allowRotating = false;
                    startAngle = getAngle(event.getX(), event.getY());
                    break;

                case MotionEvent.ACTION_MOVE:

                    double currentAngle = getAngle(event.getX(), event.getY());
                    rotateBezel((float) (startAngle - currentAngle));
                    startAngle = currentAngle;
                    break;

                case MotionEvent.ACTION_UP:

                    allowRotating = true;
                    break;
            }

            // set the touched quadrant to true
            quadrantTouched[getQuadrant(event.getX() - (dialerWidth / 2), dialerHeight - event.getY() - (dialerHeight / 2))] = true;

            mGestureDetector.onTouchEvent(event);

            return true;
        }

    }

    //##############################################################################################
    //
    //  Simple implementation of a {@link SimpleOnGestureListener} for detecting a fling event.
    //
    //##############################################################################################

    private class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            //  Dampen the fling velocity to prevent "Roulette Wheel" spin
            velocityX /= 32f;
            velocityY /= 32f;


            // get the quadrant of the start and the end of the fling
            int q1 = getQuadrant(e1.getX() - (dialerWidth / 2), dialerHeight - e1.getY() - (dialerHeight / 2));
            int q2 = getQuadrant(e2.getX() - (dialerWidth / 2), dialerHeight - e2.getY() - (dialerHeight / 2));

            // the inversed rotations
            if ((q1 == 2 && q2 == 2 && Math.abs(velocityX) < Math.abs(velocityY))
                    || (q1 == 3 && q2 == 3)
                    || (q1 == 1 && q2 == 3)
                    || (q1 == 4 && q2 == 4 && Math.abs(velocityX) > Math.abs(velocityY))
                    || ((q1 == 2 && q2 == 3) || (q1 == 3 && q2 == 2))
                    || ((q1 == 3 && q2 == 4) || (q1 == 4 && q2 == 3))
                    || (q1 == 2 && q2 == 4 && quadrantTouched[3])
                    || (q1 == 4 && q2 == 2 && quadrantTouched[3])){

                mBezel.post(new FlingRunnable(-1 * (velocityX + velocityY)));

            }
            else {

                mBezel.post(new FlingRunnable(velocityX + velocityY));
            }
            return true;
        }
    }

    //##############################################################################################
    //
    //  {@link Runnable} for animating the the dialer's fling.
    //
    private class FlingRunnable implements Runnable {

        private float velocity;

        public FlingRunnable(float velocity) {
            this.velocity = velocity;
        }

        @Override
        public void run() {
            if (Math.abs(velocity) > 5) {
                rotateBezel(velocity / 75);
                velocity /= 1.0666F;

                // post this instance again
                mBezel.post(this);
            }
        }
    }
}

