/*******************************************************************************
 * Copyright (c) 2015 to present.   JTS Interactive and John Sentz developer.  All rights reserved.
 ******************************************************************************/

package com.jtsinteractive.tritiumcompass;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;


public class NoticeDialogFragment extends DialogFragment {

    private String EULA_PREFIX = "eula_";
    private String DONTSHOW = "DON'T SHOW";
    private Activity mActivity;
    private CheckBox mCheckBox;
    private boolean mDontShowAgain;


    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;


    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        //#########################################################################################
        //
        //  Get the context from the calling activity
        //
        mActivity = activity;

        //#########################################################################################
        //
        // Verify that the host activity implements the callback interface
        //
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //#########################################################################################
        //
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.calibration_alert, null);

        //#########################################################################################
        //
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        //
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                    }
                });

        //#########################################################################################
        //
        //  Assign Don't show again checkbox object and create listener
        //
        mCheckBox = (CheckBox)view.findViewById(R.id.alertCheckBox);
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (mCheckBox.isChecked()) {
                    mDontShowAgain = true;

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean(DONTSHOW, true);
                    editor.commit();
                }
            }
        });

        return builder.create();
    }

}
